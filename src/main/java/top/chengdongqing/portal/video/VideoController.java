package top.chengdongqing.portal.video;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;

/**
 * 视频相关控制器
 * 
 * @author Luyao
 *
 */
public class VideoController extends Controller {

	@Inject
	VideoService videoSrv;

	/**
	 * 获取视频列表
	 */
	@Before(GET.class)
	public void index() {
		renderJson(videoSrv.findAll(getBoolean("all", false)));
	}
}
