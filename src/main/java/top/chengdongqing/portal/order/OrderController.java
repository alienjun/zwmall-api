package top.chengdongqing.portal.order;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;

import top.chengdongqing.common.annotation.NotBlank;
import top.chengdongqing.common.controller.BaseController;

/**
 * 订单相关控制器
 * 
 * @author Luyao
 *
 */
public class OrderController extends BaseController {

	@Inject
	OrderService orderSrv;

	/**
	 * 查询订单列表
	 */
	@Before(GET.class)
	public void index() {
		int status = getInt("status", 0);
		String keyword = get("keyword", "all");
		int pageNumber = getInt("pageNumber", 1);
		int pageSize = getInt("pageSize", 10);
		renderJson(orderSrv.paginate(status, keyword, pageNumber, pageSize, getUserId()));
	}

	/**
	 * 删除订单
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(POST.class)
	public void delete(int id) {
		renderJson(orderSrv.delete(id, getUserId()));
	}

	/**
	 * 查询订单详情
	 * 
	 * @param orderNo
	 */
	@Before(GET.class)
	@NotBlank("orderNo")
	public void details(String orderNo) {
		renderJson(orderSrv.findDetails(orderNo, getUserId()));
	}

	/**
	 * 取消订单
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(POST.class)
	public void cancel(int id) {
		renderJson(orderSrv.cancelOrder(id, getUserId()));
	}

	/**
	 * 申请退款
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(POST.class)
	public void refund(int id) {
		renderJson(orderSrv.refundOrder(id, getUserId()));
	}

	/**
	 * 确认收货
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(POST.class)
	public void confirmReceipt(int id) {
		renderJson(orderSrv.confirmReceipt(id, getUserId()));
	}
}
