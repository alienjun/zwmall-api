package top.chengdongqing.portal.goods;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.plugin.ehcache.CacheInterceptor;

import top.chengdongqing.common.annotation.NotBlank;

/**
 * 商品相关控制器
 * 
 * @author Luyao
 *
 */
public class GoodsController extends Controller {

	@Inject
	GoodsService goodsSrv;

	/**
	 * 查询最新的10个商品
	 */
	@Before({ GET.class, CacheInterceptor.class })
	public void hotGoodses() {
		renderJson(goodsSrv.findHotGoodses());
	}

	/**
	 * 获取搜索提示词
	 * 
	 * @param keyword 搜索关键词
	 */
	@Before(GET.class)
	@NotBlank("keyword")
	public void promptKeywords(String keyword) {
		renderJson(goodsSrv.findPromptKeywords(keyword));
	}

	/**
	 * 商品搜索
	 */
	@Before(GET.class)
	public void search() {
		String keyword = get("keyword", "all");
		int categoryId = getInt("categoryId", -1);
		int brandId = getInt("brandId", -1);
		String sort = get("sort", "new");
		int pageNumber = getInt("pageNumber", 1);
		int pageSize = getInt("pageSize", 20);
		renderJson(goodsSrv.searchGoodses(keyword, categoryId, brandId, sort, pageNumber, pageSize));
	}

	/**
	 * 获取随机推荐的商品
	 */
	@Before(GET.class)
	public void recommends() {
		renderJson(goodsSrv.findRecommends());
	}

	/**
	 * 查询商品详情
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(GET.class)
	public void details(int id) {
		renderJson(goodsSrv.findGoodsDetail(id));
	}

	/**
	 * 获取商品规格
	 * 
	 * @param id
	 */
	@NotBlank
	@Before(GET.class)
	public void skus(int id) {
		renderJson(goodsSrv.findSkus(id));
	}
}
