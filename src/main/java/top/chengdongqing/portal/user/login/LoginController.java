package top.chengdongqing.portal.user.login;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.ActionKey;
import com.jfinal.ext.interceptor.POST;

import top.chengdongqing.common.annotation.AccessLimit;
import top.chengdongqing.common.annotation.NotBlank;
import top.chengdongqing.common.controller.BaseController;
import top.chengdongqing.common.interceptor.LoginAuthInterceptor;
import top.chengdongqing.common.sender.SmsSender;
import top.chengdongqing.portal.user.login.validator.AccountLoginValidator;
import top.chengdongqing.portal.user.login.validator.CaptchaLoginValidator;

/**
 * 登录相关控制器
 * 
 * @author Luyao
 *
 */
@Clear(LoginAuthInterceptor.class)
public class LoginController extends BaseController {

	@Inject
	LoginService loginSrv;

	/**
	 * 账号密码登录
	 * 
	 * @param account
	 * @param password
	 */
	@AccessLimit(expireTime = 60, times = 5)
	@Before({ POST.class, AccountLoginValidator.class })
	public void index(String account, String password) {
		renderJson(loginSrv.accountLogin(account, password, getIp()));
	}

	/**
	 * 手机号加短信验证码登录
	 * 
	 * @param phone
	 */
	@AccessLimit(expireTime = 60, times = 3)
	@Before({ POST.class, CaptchaLoginValidator.class })
	public void captchaLogin(String phone) {
		renderJson(loginSrv.captchaLogin(phone, getIp()));
	}

	/**
	 * 发送短信验证码
	 * 
	 * @param phone
	 */
	@NotBlank("phone")
	@Before(POST.class)
	@AccessLimit(limitKey = "phone", expireTime = 60 * 60 * 24, times = 5, msg = "今日发送次数超出，请明日再试")
	public void sendSMSCaptcha(String phone) {
		renderJson(SmsSender.getInstance().sendCaptcha(phone, SmsSender.Template.LOGIN));
	}

	/**
	 * 退出登录
	 */
	@ActionKey("logout")
	@Before({ POST.class, LoginAuthInterceptor.class })
	public void logout() {
		renderJson(loginSrv.logout(getUser()));
	}
}
