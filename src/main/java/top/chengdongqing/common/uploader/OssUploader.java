package top.chengdongqing.common.uploader;

import java.nio.file.Path;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

import top.chengdongqing.common.config.Constant.ConfigFile;

/**
 * 上传文件到OSS
 * 
 * @author Luyao
 *
 */
public class OssUploader extends Uploader {

	// 当前类的实例
	private static final Uploader me = new OssUploader();

	private OssUploader() {

	}

	public static Uploader getInstance() {
		return me;
	}

	// 配置文件
	private static final Prop prop = PropKit.use(ConfigFile.OSS);

	@Override
	String upload(Path filePath, String saveDire, String fileName) {
		// 实例化oss客户端
		OSS client = new OSSClientBuilder().build(prop.get("endpoint"), prop.get("access.id"),
				prop.get("access.secret"));
		// 拼接对象的键名
		String objectName = saveDire + "/" + fileName;

		// 新建上传文件请求
		PutObjectRequest request = new PutObjectRequest(prop.get("bucket"), objectName, filePath.toFile());
		// 上传文件
		client.putObject(request);
		// 关闭客户端
		client.shutdown();

		// 拼接文件完整路径
		StringBuilder fullUrl = new StringBuilder("https://");
		fullUrl.append(prop.get("bucket")).append(".").append(prop.get("endpoint"));
		return fullUrl.append("/").append(objectName).toString();
	}
}
